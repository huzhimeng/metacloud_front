/**
 * 关于一些过滤组件的编写
 */
import moment from 'moment'

// 时间格式化组件
const timeFormat = (Vue) => {
  Vue.filter('timeformat', (value, fmtstring) => {
    if (value) {
      if (isNaN(value)) {
        if (value.indexOf('期') !== -1) {
          return value
        } else {
          return moment(value).format(fmtstring)
        }
      } else {
        return moment((value + moment().utcOffset() * 60) * 1000).format(fmtstring)
      }
    }
  })
}

// 文件来源格式化组件
const setBracket = (Vue) => {
  Vue.filter('setbracket', (value) => {
    if (value) {
      if ( value.indexOf('(') !== -1 ) {
        return value.substr(0, value.indexOf('('))
      } else if (value.indexOf('（') !== -1) {
        return value.substr(0, value.indexOf('（'))
      } else {
        return value
      }
    }
  })
}

export default (Vue) => {
  timeFormat(Vue)
  setBracket(Vue)
}
