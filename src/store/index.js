import Vue from 'vue'
import Vuex from 'vuex'
// import createLogger from 'vuex/src/plugins/logger'
import userSystem from './modules/user_system'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    userSystem
  },
  strict: debug,
  // plugins: debug ? [createLogger()] : []
})
