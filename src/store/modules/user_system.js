import * as types from '../mutation-types'
import Vue from 'vue'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

// init state
const state = {
  user: (!!localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).uid) ? JSON.parse(localStorage.getItem('user')) : null,
  isLogin: (!!localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).uid) ? JSON.parse(localStorage.getItem('user')) : false,
  resError: '',
  users: [],
  registerError: ''
}

const mutations = {

}

const actions = {
}

function clearCookies () {
  $cookies.set('uid', '0', '0s', null, config.domain)
  $cookies.set('register', '0', '0s', null, config.domain)
  $cookies.set('access_key', '0', '0s', null, config.domain)
  $cookies.set('cloud_user', '0', '0s', null, config.domain)
  localStorage.removeItem('user')
}

export default {
  state,
  mutations,
  actions
}
